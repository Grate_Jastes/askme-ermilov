"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URL conf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('settings/', views.settings_page, name='settings'),
    path('login/', views.login, name='login'),
    path('logout/', views.log_out, name='logout'),
    path('ask/', views.ask, name='ask'),
    path('question/<int:idk>/', views.question_page, name='question'),
    path('register/', views.register, name='register'),
    path('tag/<str:tag_name>/', views.find_by_tag, name='tag'),
    path('hot/', views.hot, name='hot'),
    path('leave_vote/', views.leave_vote, name='leave_vote'),
    path('mark_correct/', views.mark_correct, name='mark_correct'),


    # path('uploads/', views.uploads, name='uploads')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
