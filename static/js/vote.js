$('.js-vote').click(function (ev) {
    ev.preventDefault()
    var $this = $(this),
        action = $this.data('action'),
        instance_id = $this.data('instance_id'),
        instance_type = $this.data('instance_type');
    $.ajax('/leave_vote/', {
        method: 'POST',
        data: {
            action: action,
            instance_id: instance_id,
            instance_type: instance_type,
        },
        success : function(json) {
            document.getElementById(
                json.instance_type + "_" + $this.data('instance_id')
            ).innerHTML = json.rating;
        }
    }).done(function(data) {
        console.log("DATA: " + data)
    })

    console.log("HERE: " + action + " " + instance_id);
});

$('.js-correct').click(function (ev) {
    // ev.preventDefault()
    var $this = $(this),
        answer_id = $this.data('aid'),
        question_id = $this.data('qid');
    $.ajax('/mark_correct/', {
        method: 'POST',
        data: {
            answer_id: answer_id,
            question_id: question_id,
        }
    })
})

