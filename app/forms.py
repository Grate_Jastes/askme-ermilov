from django import forms
from django.contrib.auth.forms import UserCreationForm
from app.models import *


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class AskForm(forms.ModelForm):
    tags = forms.CharField(
        required=False, max_length=200,
        label='Tags (5 max)',
        widget=forms.TextInput(attrs={
            'placeholder': 'Enter your tags, separated by whitespace',
        }
        )
    )

    class Meta:
        model = Question
        fields = ['title', 'text']


class RegisterForm(UserCreationForm):
    nickname = forms.CharField(help_text='Your nickname on the site', max_length=32)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
            'first_name',
            'last_name'
        ]


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['text', ]


class SettingsForm(UserCreationForm):
    nickname = forms.CharField(help_text='Your nickname on the site',
                               max_length=32,
                               required=False,
                               widget=forms.TextInput()
                               )

    avatar = forms.ImageField()

    def __init__(self, *args, **kwargs):
        if 'user' in kwargs:
            user = kwargs.pop('user')
            super().__init__(*args, **kwargs)

            self.fields['username'].widget.attrs['placeholder'] = user.username
            self.fields['email'].widget.attrs['placeholder'] = user.email
            self.fields['nickname'].widget.attrs['placeholder'] = user.profile.nickname
            self.fields['first_name'].widget.attrs['placeholder'] = user.first_name
            self.fields['last_name'].widget.attrs['placeholder'] = user.last_name
        else:
            super().__init__(*args, **kwargs)

        self.fields['username'].required = False
        self.fields['email'].required = False
        self.fields['password1'].required = False
        self.fields['password2'].required = False
        self.fields['first_name'].required = False
        self.fields['last_name'].required = False

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
            'first_name',
            'last_name'
        ]
