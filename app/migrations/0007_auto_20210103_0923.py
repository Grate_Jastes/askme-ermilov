# Generated by Django 3.1.4 on 2021-01-03 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_answer_rating'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='name',
            field=models.SlugField(max_length=32, unique=True, verbose_name='Tag value'),
        ),
    ]
