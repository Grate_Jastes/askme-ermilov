from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Django user')
    nickname = models.CharField(verbose_name='Nickname', max_length=32)
    avatar = models.ImageField(
        upload_to='avatars/%Y/%m/%d',
        default='avatars/default.jpg',
        verbose_name='Profile avatar',
    )

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class QuestionManager(models.Manager):
    def has_tag(self, tag):
        return Question.objects.filter(tags__name__exact=tag.lower())

    def new(self):
        return Question.objects.order_by('-creation_date')

    def hot(self):
        return Question.objects.order_by('-rating')


class Question(models.Model):
    author = models.ForeignKey('Profile', on_delete=models.CASCADE, verbose_name='Asked by')
    title = models.CharField(max_length=256, verbose_name='Question title')
    text = models.TextField(verbose_name='Question text')
    tags = models.ManyToManyField('Tag', verbose_name='Question tags')
    rating = models.IntegerField(verbose_name='Rating', default=0)
    creation_date = models.DateTimeField(auto_now_add=False, verbose_name='Date of asking')  # auto_now_add=True

    objects = QuestionManager()

    def refresh_rating(self):
        self.rating = QuestionVote.objects.rating(question=self)
        self.save()

    def answer_count(self):
        return Answer.objects.filter(question=self).count()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Question'
        verbose_name_plural = 'Questions'


class Answer(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE, verbose_name='Answered to question')
    text = models.TextField(verbose_name='Answer text')
    author = models.ForeignKey('Profile', on_delete=models.CASCADE, verbose_name='Answered by')
    creation_date = models.DateTimeField(auto_now_add=False, verbose_name='Date of answer')  # auto_now_add=True
    correct = models.BooleanField(default=False, verbose_name='Answer is correct')
    rating = models.IntegerField(default=0, verbose_name='Rating')

    def refresh_rating(self):
        self.rating = AnswerVote.objects.rating(answer=self)
        self.save()

    def __str__(self):
        return self.id.__str__()

    class Meta:
        verbose_name = 'Answer'
        verbose_name_plural = 'Answers'


class TagManager(models.Manager):
    def popular(self):
        return Tag.objects.order_by('-rating')[:5]


class Tag(models.Model):
    name = models.SlugField(max_length=32, unique=True, verbose_name='Tag value')
    rating = models.IntegerField(default=0)

    objects = TagManager()

    def refresh_rating(self):
        self.rating = Question.objects.has_tag(tag=self.name).count()
        self.save()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'


class QuestionVoteManager(models.Manager):
    def rating(self, question):
        return sum(QuestionVote.objects.filter(question_id=question.id).values_list('rating_change', flat=True))


class QuestionVote(models.Model):
    rating_change = models.SmallIntegerField(verbose_name='Rating_change', default=0)
    question = models.ForeignKey('Question', on_delete=models.CASCADE, verbose_name='Which question is voted by')
    author = models.ForeignKey('Profile', on_delete=models.PROTECT, verbose_name='Who voted')

    objects = QuestionVoteManager()

    def __str__(self):
        if self.rating_change > 0:
            return 'Like'
        else:
            return 'Dislike'

    class Meta:
        verbose_name = 'Question vote'
        verbose_name_plural = 'Question votes'


class AnswerVoteManager(models.Manager):
    def rating(self, answer):
        return sum(AnswerVote.objects.filter(answer_id=answer.id).values_list('rating_change', flat=True))


class AnswerVote(models.Model):
    rating_change = models.SmallIntegerField(verbose_name='Rating_change', default=0)
    answer = models.ForeignKey('Answer', on_delete=models.CASCADE, verbose_name='Which answer is voted by')
    author = models.ForeignKey('Profile', on_delete=models.PROTECT, verbose_name='Who voted')

    objects = AnswerVoteManager()

    def __str__(self):
        if self.rating_change:
            return 'Like'
        else:
            return 'Dislike'

    class Meta:
        verbose_name = 'Answer vote'
        verbose_name_plural = 'Answer votes'
