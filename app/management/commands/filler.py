from django.core.management.base import BaseCommand
from django.db import transaction


from itertools import islice

from django.contrib.auth.hashers import make_password
from app.models import *

from os import listdir
from os.path import isfile, join

from faker import Faker
from random import choice, choices

f = Faker()

BATCH_SIZE = 100000  # Batch size for bulk_create


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-u', '--users', type=int, help='Usernames amount')
        parser.add_argument('-q', '--questions', type=int, help='Questions amount')
        parser.add_argument('-a', '--answers', type=int, help='Answers amount')
        parser.add_argument('-t', '--tags', type=int, help='Tags amount')
        parser.add_argument('-l', '--likes', type=int, help='Total likes amount')
        parser.add_argument('-s', '--db_size', type=str, help='Preset amounts')

    def handle(self, *args, **options):
        users_cnt = options['users']
        questions_cnt = options['questions']
        answers_cnt = options['answers']
        tags_cnt = options['tags']
        votes_cnt = options['likes']
        db_size = options['db_size']

        if db_size:
            if db_size == 'small':
                users_cnt = 20
                questions_cnt = 30
                answers_cnt = 60
                tags_cnt = 10
                votes_cnt = 1000
            elif db_size == 'medium':
                users_cnt = 1000
                questions_cnt = 5000
                answers_cnt = 10000
                tags_cnt = 500
                votes_cnt = 100000
            elif db_size == 'large':
                users_cnt = 10000
                questions_cnt = 100000
                answers_cnt = 1000000
                tags_cnt = 10000
                votes_cnt = 2000000

        if users_cnt:
            self.fill_profiles(users_cnt)
            print("users done")

        if tags_cnt:
            self.fill_tags(tags_cnt)
            print('tags done')

        if questions_cnt:
            self.fill_questions(questions_cnt)

            tags = Tag.objects.all()
            max_tags = min(5, len(tags))

            batches_cnt = Tag.objects.all().count()

            for i in range(batches_cnt):
                questions_batch = \
                    Question.objects.all()[i * BATCH_SIZE: (i + 1) * BATCH_SIZE]

                with transaction.atomic():
                    for question in questions_batch:
                        question.tags.set(choices(tags, k=f.random_int(min=1, max=max_tags)))
                        question.save()

            for tag in tags:
                tag.refresh_rating()

            print('questions done')

        if answers_cnt:
            self.fill_answers(answers_cnt)
            print('answers done')

        if votes_cnt:
            self.fill_votes(votes_cnt)

            questions = Question.objects.all()
            for question in questions:
                question.refresh_rating()

            answers = Answer.objects.all()
            for answer in answers:
                answer.refresh_rating()

            print('votes done')

    def bulk_create_by_batches(self, generator, model_type, ignore_conflicts=False):

        while True:
            batch = list(islice(generator, BATCH_SIZE))
            if not batch:
                break

            model_type.objects.bulk_create(batch, BATCH_SIZE, ignore_conflicts=ignore_conflicts)

    def fill_users(self, cnt):
        user_generator = (User(
            username=f.unique.user_name(),
            email=f.unique.email(),
            password=make_password(f.password(length=f.random_int(min=8, max=12))),
            first_name=f.first_name(),
            last_name=f.last_name()
        ) for _ in range(cnt))

        self.bulk_create_by_batches(user_generator, User)

    def fill_profiles(self, cnt):
        self.fill_users(cnt)
        users = User.objects.all()

        avatars_path = 'uploads/avatars/'
        avatar_links = [filename for filename in listdir(avatars_path)
                        if isfile(join(avatars_path, filename))]

        profiles_generator = (Profile(
            user=users[i],
            avatar='avatars/' + choice(avatar_links),
            nickname=f.unique.user_name(),
        ) for i in range(cnt))

        self.bulk_create_by_batches(profiles_generator, Profile)

    def fill_questions(self, cnt):
        profiles = Profile.objects.all()

        questions_generator = (Question(
            author=choice(profiles),
            title=f.sentence(nb_words=5)[:256],
            text="".join(f.sentences(f.random_int(min=2, max=7))),
            creation_date=f.date_time_between(end_date='now')
        ) for _ in range(cnt))

        self.bulk_create_by_batches(questions_generator, Question)

    def fill_answers(self, cnt):
        profiles = Profile.objects.all()
        questions = Question.objects.all()

        answers_generator = (Answer(
            author=choice(profiles),
            question=choice(questions),
            text="".join(f.sentences(f.random_int(min=1, max=5))),
            creation_date=f.date_time_between(start_date='-30d', end_date='now')
            ) for _ in range(cnt))

        self.bulk_create_by_batches(answers_generator, Answer)

    def fill_tags(self, cnt):
        tags_generator = (Tag(
            name=f.unique.word()
        ) for _ in range(cnt))

        self.bulk_create_by_batches(tags_generator, Tag)

    def fill_votes(self, cnt):
        profiles = Profile.objects.all()
        questions = Question.objects.all()
        answers = Answer.objects.all()

        answer_votes_cnt = int(cnt / 3)
        question_votes_cnt = cnt - answer_votes_cnt

        question_votes_generator = (QuestionVote(
            author=choice(profiles),
            question=choice(questions),
            rating_change=(1 if f.pyint() % 3 < 2 else -1),
        ) for _ in range(question_votes_cnt))

        answer_votes_generator = (AnswerVote(
            author=choice(profiles),
            answer=choice(answers),
            rating_change=(1 if f.pyint() % 3 < 2 else -1),
        ) for _ in range(answer_votes_cnt))

        self.bulk_create_by_batches(answer_votes_generator, AnswerVote, True)
        self.bulk_create_by_batches(question_votes_generator, QuestionVote, True)
