from django.db import transaction
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.http import Http404, HttpResponseNotAllowed, JsonResponse, HttpResponseForbidden
from django.core.paginator import Paginator
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from datetime import datetime
from django.contrib import messages
from django.views.decorators.http import require_POST

from app.models import *
from app.forms import *


def paginate(request, found_objects, per_page):
    page_num = request.GET.get('page', 1)
    paginator = Paginator(found_objects, per_page)
    try:
        if int(page_num) > paginator.num_pages:
            raise Http404('Wrong page number')
    except ValueError:
        raise Http404('Wrong page parameter')

    return paginator.get_page(page_num)


def index(request):
    questions = Question.objects.new().prefetch_related('author', 'tags')
    popular_tags = Tag.objects.popular()
    return render(request, 'index.html', {
        'questions': paginate(request, questions, 10),
        'tags': popular_tags,
    })


def question_page(request, idk):
    question = Question.objects.filter(id=idk).first()
    answers = Answer.objects.filter(question_id=idk).order_by('creation_date')
    if request.method == 'GET':
        form = AnswerForm()
    elif request.method == 'POST':
        if not request.user.is_authenticated:
            return redirect('login')

        form = AnswerForm(data=request.POST)
        if form.is_valid():
            answer = Answer.objects.create(
                text=form.cleaned_data['text'],
                creation_date=datetime.now(),
                question=question,
                author=request.user.profile,
            )
            answer.save()
            answers = Answer.objects.filter(question_id=idk)
            paginator = Paginator(answers, 5)
            return redirect(reverse('question', kwargs={'idk': question.id}) +
                            '?page={}#answer_{}'.format(paginator.num_pages, answer.id))
    else:
        return HttpResponseNotAllowed(permitted_methods=['POST', 'GET'])

    return render(request, 'question_page.html', {
        'question': question,
        'rating': QuestionVote.objects.rating(question),
        'answers': paginate(request, answers, 5),
        'form': form,
        'tags': Tag.objects.popular(),
    })


def login(request):
    if request.method == 'GET':
        form = LoginForm()
        request.session['login_referer'] = request.META.get('HTTP_REFERER')
    elif request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = auth.authenticate(request, **form.cleaned_data)
            if user is not None:
                auth.login(request, user)
                if 'next' in request.GET:
                    return redirect(request.GET.get('next'))
                elif 'login_referer' in request.session:
                    referer = request.session.get('login_referer')
                    request.session.pop('login_referer')
                    return redirect(referer)
                else:
                    return redirect('/')
            else:
                messages.error(request, 'User with such username and password not found!')
        else:
            messages.error(request, 'Error in fields')
    else:
        return HttpResponseNotAllowed(permitted_methods=['POST', 'GET'])

    m = messages.get_messages(request)
    return render(request, 'login.html', {
        'form': form,
        'messages': m,
        'tags': Tag.objects.popular(),
    })


def make_question(request, form):
    question = form.save(commit=False)
    question.author = request.user.profile
    question.creation_date = datetime.now()

    tag_names = list(set(form.cleaned_data['tags'].split(' ')))
    with transaction.atomic():
        for tag_name in tag_names:
            if not Tag.objects.filter(name=tag_name).exists():
                Tag.objects.create(name=tag_name)
    question.save()
    tags = Tag.objects.filter(name__in=tag_names)
    question.tags.set(tags)
    return question


@login_required
def ask(request):
    if request.method == 'GET':
        form = AskForm()
    elif request.method == 'POST':
        form = AskForm(data=request.POST)
        if form.is_valid():
            question = make_question(request, form)

            return redirect(reverse('question', kwargs={'idk': question.id}))
    else:
        return HttpResponseNotAllowed(permitted_methods=['POST', 'GET'])

    return render(request, 'ask.html', {
        'form': form,
        'tags': Tag.objects.popular(),
    })


@login_required
def settings_page(request):
    popular_tags = Tag.objects.popular()
    if request.method == 'GET':
        form = SettingsForm(user=request.user)
    elif request.method == 'POST':
        form = SettingsForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            if 'username' in form.changed_data:
                request.user.username = form.cleaned_data['username']
            if 'email' in form.changed_data:
                request.user.email = form.cleaned_data['email']
            if 'nickname' in form.changed_data:
                request.user.profile.nickname = form.cleaned_data['nickname']
            if 'first_name' in form.changed_data:
                request.user.first_name = form.cleaned_data['first_name']
            if 'last_name' in form.changed_data:
                request.user.last_name = form.cleaned_data['last_name']
            if 'password' in form.changed_data:
                request.user.set_password(form.cleaned_data['nickname'])
            if 'avatar' in form.changed_data:
                request.user.profile.avatar = form.cleaned_data['avatar']

            request.user.save()
    else:
        return HttpResponseNotAllowed(permitted_methods=['POST', 'GET'])

    return render(request, 'settings.html', {
        'tags': popular_tags,
        'form': form,
    })


def register(request):
    if request.method == 'GET':
        form = RegisterForm()
    elif request.method == 'POST':
        form = RegisterForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.nickname = form.cleaned_data.get('nickname')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            auth.login(request, user)
            return redirect('/')
    else:
        return HttpResponseNotAllowed(permitted_methods=['POST', 'GET'])

    return render(request, 'register.html', {'form': form})


def hot(request):
    questions = Question.objects.hot().prefetch_related('author', 'tags')
    popular_tags = Tag.objects.popular()
    return render(request, 'hot.html', {
        'questions': paginate(request, questions, 10),
        'tags': popular_tags,
    })


def find_by_tag(request, tag_name):
    found_questions = Question.objects.has_tag(tag_name)
    paginator = Paginator(found_questions, 5)
    popular_tags = Tag.objects.popular()
    return render(request, 'tag.html', {
        'tag_name': tag_name,
        'questions': paginator.get_page(request.GET.get('page')),
        'count': paginator.count,
        'tags': popular_tags,
    })


@login_required
def log_out(request):
    auth.logout(request)
    return redirect(request.META.get('HTTP_REFERER'))


@require_POST
@login_required
def leave_vote(request):
    data = request.POST

    if data.get('instance_type') == 'question':
        voted_instance = Question.objects.get(id=data.get('instance_id'))
        vote, created = QuestionVote.objects.get_or_create(
            question=voted_instance,
            author=request.user.profile,
        )
    else:
        voted_instance = Answer.objects.get(id=data.get('instance_id'))
        vote, created = AnswerVote.objects.get_or_create(
            answer=voted_instance,
            author=request.user.profile,
        )

    vote.rating_change = 1 if data.get('action') == 'like' else -1
    vote.save()

    voted_instance.refresh_rating()
    return JsonResponse({'rating': voted_instance.rating,
                         'instance_type': data.get('instance_type'),
                         })


@require_POST
@login_required
def mark_correct(request):
    data = request.POST

    question = Question.objects.get(id=data.get('question_id'))
    if question.author.id != request.user.profile.id:
        return HttpResponseForbidden()

    answer = Answer.objects.get(id=data.get('answer_id'))

    answer.correct = False if answer.correct else True
    answer.save()

    return JsonResponse({})


